package cr.ac.ucenfotec.dao;

import cr.ac.ucenfotec.bl.entities.carrera.ICarreraDAO;
import cr.ac.ucenfotec.bl.entities.carrera.MemoryCarreraImpl;
import cr.ac.ucenfotec.bl.entities.curso.ICursoDAO;
import cr.ac.ucenfotec.bl.entities.curso.MemoryCursoImpl;
import cr.ac.ucenfotec.bl.entities.estudiante.IEstudianteDAO;
import cr.ac.ucenfotec.bl.entities.estudiante.MemoryEstudianteImpl;
import cr.ac.ucenfotec.bl.entities.grupo.IGrupoDAO;
import cr.ac.ucenfotec.bl.entities.grupo.MemoryGrupoImpl;

public class MemoryDaoFactory extends DaoFactory{

    public ICarreraDAO getICarreraDao() {
        return new MemoryCarreraImpl();
    }

    public ICursoDAO getICursoDao() {
        return new MemoryCursoImpl();
    }

    public IEstudianteDAO getIEstudianteDao() {
        return new MemoryEstudianteImpl();
    }

    public IGrupoDAO getIGrupoDao() {
        return new MemoryGrupoImpl();
    }
}
