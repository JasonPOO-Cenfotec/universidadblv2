package cr.ac.ucenfotec.dao;

import cr.ac.ucenfotec.bl.entities.carrera.ICarreraDAO;
import cr.ac.ucenfotec.bl.entities.curso.ICursoDAO;
import cr.ac.ucenfotec.bl.entities.estudiante.IEstudianteDAO;
import cr.ac.ucenfotec.bl.entities.grupo.IGrupoDAO;
import cr.ac.ucenfotec.util.Utils;

public abstract class DaoFactory {

    public static DaoFactory getDaoFactory() {
        String repository;
        try {
            repository = Utils.getProperties()[6];
            switch (repository){
                case "MYSQL": return new MySqlDaoFactory();
                case "MEMORY": return new MemoryDaoFactory();
                default: return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public abstract ICarreraDAO getICarreraDao();
    public abstract ICursoDAO getICursoDao();
    public abstract IEstudianteDAO getIEstudianteDao();
    public abstract IGrupoDAO getIGrupoDao();
}
