package cr.ac.ucenfotec.dao;

import cr.ac.ucenfotec.bl.entities.carrera.ICarreraDAO;
import cr.ac.ucenfotec.bl.entities.carrera.MySQLCarreraImpl;
import cr.ac.ucenfotec.bl.entities.curso.ICursoDAO;
import cr.ac.ucenfotec.bl.entities.curso.MySQLCursoImpl;
import cr.ac.ucenfotec.bl.entities.estudiante.IEstudianteDAO;
import cr.ac.ucenfotec.bl.entities.estudiante.MySqlEstudianteImpl;
import cr.ac.ucenfotec.bl.entities.grupo.IGrupoDAO;
import cr.ac.ucenfotec.bl.entities.grupo.MySQLGrupoImpl;

public class MySqlDaoFactory extends DaoFactory{

    public ICarreraDAO getICarreraDao() {
        return new MySQLCarreraImpl();
    }

    public ICursoDAO getICursoDao() {
        return new MySQLCursoImpl();
    }

    public IEstudianteDAO getIEstudianteDao() {
        return new MySqlEstudianteImpl();
    }

    public IGrupoDAO getIGrupoDao() {
        return new MySQLGrupoImpl();
    }
}
