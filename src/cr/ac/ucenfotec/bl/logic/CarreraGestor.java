package cr.ac.ucenfotec.bl.logic;

import cr.ac.ucenfotec.bl.entities.carrera.Carrera;
import cr.ac.ucenfotec.bl.entities.carrera.ICarreraDAO;
import cr.ac.ucenfotec.bl.entities.curso.Curso;
import cr.ac.ucenfotec.bl.entities.curso.ICursoDAO;
import cr.ac.ucenfotec.dao.DaoFactory;

import java.util.ArrayList;

public class CarreraGestor {

    private DaoFactory factory;
    private ICarreraDAO datos;
    private ICursoDAO datosCurso;

    public CarreraGestor(){
        factory = DaoFactory.getDaoFactory();
        datos = factory.getICarreraDao();
        datosCurso = factory.getICursoDao();
    }

    public String registrarCarrera(String codigo,String nombre, boolean acreditada) throws Exception{
        Carrera tmpCarrera = new Carrera(codigo,nombre,acreditada);

        return datos.registrarCarrera(tmpCarrera);
    }

    public ArrayList<Carrera> getCarreras() throws Exception{
        return datos.listarCarreras();
    }

    public String asociarCurso(String codigoCurso,String codigoCarrera) throws Exception{
        // 1. Buscar el curso en la BD con el código
          Curso curso = datosCurso.buscarCurso(codigoCurso);
          if(curso != null){
              // 3. Si existe, buscar la carrera en la BD con el código
              Carrera carrera = datos.buscarCarrera(codigoCarrera);
              if(carrera != null){
                  // 5. Si existe, vamos a registra la asociación en la tabla carrera_curso
                  return datos.asociarCursoACarrera(carrera,curso);
              }else{
                  // 4. Si NOO existe, devuelvo un mensaje de error.
                  return "El código de la carrera ingresado no existe en la BD";
              }
          }else{
              // 2. Si NOO existe devuelvo un mensaje de error.
              return "El código de curso ingresado no existe en la BD";
          }
    }
}
