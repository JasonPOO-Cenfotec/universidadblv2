package cr.ac.ucenfotec.bl.logic;

import cr.ac.ucenfotec.bl.entities.curso.Curso;
import cr.ac.ucenfotec.bl.entities.curso.ICursoDAO;
import cr.ac.ucenfotec.bl.entities.grupo.IGrupoDAO;
import cr.ac.ucenfotec.dao.DaoFactory;

import java.util.ArrayList;

public class CursoGestor {

    private DaoFactory factory;
    private ICursoDAO datos;
    private IGrupoDAO datosGrupo;

    public CursoGestor(){
        factory = DaoFactory.getDaoFactory();
        datos = factory.getICursoDao();
        datosGrupo = factory.getIGrupoDao();
    }

    public String registrarCurso(String codigo, String nombre, int creditos) throws Exception{
        Curso tmpCurso = new Curso(codigo,nombre,creditos);
        return datos.registrarCurso(tmpCurso);
    }

    public ArrayList<Curso> getCursos() throws Exception{
        return datos.listarCursos();
    }

    public String registrarGrupo(String codigoCurso, String codigoGrupo, int numero) throws Exception{
        Curso curso = datos.buscarCurso(codigoCurso);
        if(curso !=null){
            return datosGrupo.registrarGrupo(curso,codigoGrupo,numero);
        }else{
            return "El código del curso ingresado no existe en el sistema!";
        }
    }
}
