package cr.ac.ucenfotec.bl.logic;

import cr.ac.ucenfotec.bl.entities.estudiante.Estudiante;
import cr.ac.ucenfotec.bl.entities.estudiante.IEstudianteDAO;
import cr.ac.ucenfotec.dao.DaoFactory;

import java.time.LocalDate;
import java.util.ArrayList;

public class EstudianteGestor {

    private DaoFactory factory;
    private IEstudianteDAO datos;

    public EstudianteGestor(){
        factory = DaoFactory.getDaoFactory();
        datos = factory.getIEstudianteDao();
    }

    public String agregarEstudiante(String identificacion, String nombre, String correo, int edad, LocalDate fechaNacimiento)throws Exception{
        Estudiante estudiante = new Estudiante(identificacion,nombre,correo,edad,fechaNacimiento);
        return datos.agregarEstudiante(estudiante);
    }

    public ArrayList<Estudiante> listarEstudiantes() throws Exception{
        return datos.listarEstudiantes();
    }

}
