package cr.ac.ucenfotec.bl.entities.grupo;

import java.util.Objects;

public class Grupo {

    private int numero;
    private String codigo;

    public Grupo() {
    }

    public Grupo(int numero, String codigo) {
        this.numero = numero;
        this.codigo = codigo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String toString() {
        return "Grupo{" +
                "numero=" + numero +
                ", codigo='" + codigo + '\'' +
                '}';
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Grupo grupo = (Grupo) o;
        return Objects.equals(codigo, grupo.codigo);
    }
}
