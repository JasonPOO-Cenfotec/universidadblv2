package cr.ac.ucenfotec.bl.entities.grupo;

import cr.ac.ucenfotec.bl.entities.curso.Curso;

public interface IGrupoDAO {

   String registrarGrupo(Curso curso, String codigoGrupo, int numero) throws Exception;
}
