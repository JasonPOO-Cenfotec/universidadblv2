package cr.ac.ucenfotec.bl.entities.carrera;

import cr.ac.ucenfotec.bl.entities.curso.Curso;
import java.util.ArrayList;

public interface ICarreraDAO {

    String registrarCarrera(Carrera carrera) throws Exception;
    ArrayList<Carrera> listarCarreras() throws Exception;
    Carrera buscarCarrera(String codigoCarrera) throws Exception;
    String asociarCursoACarrera(Carrera carrera, Curso curso) throws Exception;
}
