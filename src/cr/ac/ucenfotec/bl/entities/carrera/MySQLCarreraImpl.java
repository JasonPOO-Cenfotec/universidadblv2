package cr.ac.ucenfotec.bl.entities.carrera;

import cr.ac.ucenfotec.bl.entities.curso.Curso;
import cr.ac.ucenfotec.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySQLCarreraImpl implements ICarreraDAO {

    private String sqlQuery = "";

    public String registrarCarrera(Carrera carrera) throws Exception {
        sqlQuery ="INSERT INTO CARRERA VALUES('"+carrera.getCodigo()+"','"+carrera.getNombre()+"',"+carrera.isEstaAcreditada()+")";
        Conector.getConnector().ejecutarSQL(sqlQuery);

        return "La Carrera de " + carrera.getNombre() + ", fue registrada correctamente!";
    }

    public ArrayList<Carrera> listarCarreras() throws Exception {
        ArrayList<Carrera> listaCarreras = new ArrayList<>();

        sqlQuery = "SELECT CODIGOCARRERA, NOMBRE,ESTAACREDITADA FROM CARRERA";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);
        while(rs.next()){
            Carrera carrera = new Carrera(rs.getString("CODIGOCARRERA"),rs.getString("NOMBRE"),rs.getBoolean("ESTAACREDITADA"));
             sqlQuery = "SELECT CU.CODIGOCURSO,CU.NOMBRE,CU.CREDITOS " +
                     " FROM CURSO CU JOIN CARRERA_CURSO CC " +
                     " ON CU.CODIGOCURSO = CC.CODIGOCURSO " +
                     " AND CC.CODIGOCARRERA='"+carrera.getCodigo()+"'";
            ResultSet rsCurso = Conector.getConnector().ejecutarQuery(sqlQuery);
            while(rsCurso.next()){
                Curso curso = new Curso(rsCurso.getString("CODIGOCURSO"),rsCurso.getString("NOMBRE"),rsCurso.getInt("CREDITOS"));
                carrera.agregarCurso(curso);
            }
            listaCarreras.add(carrera);
        }
        return listaCarreras;
    }

    public Carrera buscarCarrera(String codigoCarrera) throws Exception {
        Carrera carrera = null;
        sqlQuery="SELECT CODIGOCARRERA, NOMBRE,ESTAACREDITADA FROM CARRERA WHERE CODIGOCARRERA='"+codigoCarrera+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        if(rs.next()){
            carrera = new Carrera(rs.getString("CODIGOCARRERA"),rs.getString("NOMBRE"),rs.getBoolean("ESTAACREDITADA"));
        }
        return carrera;
    }

    public String asociarCursoACarrera(Carrera carrera, Curso curso) throws Exception {
        sqlQuery="INSERT INTO CARRERA_CURSO VALUES ('"+carrera.getCodigo()+"','"+curso.getCodigo()+"')";
        Conector.getConnector().ejecutarSQL(sqlQuery);

        return "El curso "+ curso.getNombre() +
                ", fue asociado de manera correcta con la Carrera "+ carrera.getNombre();
    }
}
