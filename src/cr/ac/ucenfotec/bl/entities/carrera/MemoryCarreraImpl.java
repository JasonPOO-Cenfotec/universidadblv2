package cr.ac.ucenfotec.bl.entities.carrera;

import cr.ac.ucenfotec.bl.entities.curso.Curso;
import cr.ac.ucenfotec.dl.memory.Data;

import java.util.ArrayList;

public class MemoryCarreraImpl implements ICarreraDAO {
    private Data data = new Data();

    public String registrarCarrera(Carrera carrera) throws Exception {
        data.registrarCarrera(carrera);
        return "La Carrera se registro de manera correcta (Memory)";
    }

    public ArrayList<Carrera> listarCarreras() throws Exception {
        return data.listarCarreras();
    }

    public Carrera buscarCarrera(String codigoCarrera) throws Exception {
        return null;
    }

    public String asociarCursoACarrera(Carrera carrera, Curso curso) throws Exception {
        return "El curso fue agregado de manera correcta en la carrera (Memory)";
    }
}
