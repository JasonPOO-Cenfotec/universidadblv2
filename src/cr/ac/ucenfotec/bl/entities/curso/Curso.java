package cr.ac.ucenfotec.bl.entities.curso;

import cr.ac.ucenfotec.bl.entities.grupo.Grupo;

import java.util.ArrayList;

public class Curso {

    private String codigo;
    private String nombre;
    private int creditos;
    private ArrayList<Grupo> listaGrupos;

    public Curso() {
    }

    public Curso(String codigo, String nombre, int creditos) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.creditos = creditos;
        this.listaGrupos = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public void agregarGrupo(int numero, String codigo){
        Grupo grupo = new Grupo(numero,codigo);
        listaGrupos.add(grupo);
    }

    public String getInfoGrupos(){
        String info= "\n---- Grupos ---\n";
        for (Grupo  grupo: listaGrupos) {
            info += grupo.toString() + "\n";
        }
        return info;
    }

    public String toString() {
        return "--- Información del curso ---" +
                "\ncodigo=" + codigo +
                "\nnombre=" + nombre +
                "\ncreditos=" + creditos + getInfoGrupos();
    }
}
