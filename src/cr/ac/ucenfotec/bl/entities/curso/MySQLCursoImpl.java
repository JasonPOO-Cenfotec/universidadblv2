package cr.ac.ucenfotec.bl.entities.curso;

import cr.ac.ucenfotec.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySQLCursoImpl implements ICursoDAO {

    private String sqlQuery="";

    public String registrarCurso(Curso curso) throws Exception {
        sqlQuery="INSERT INTO CURSO VALUES('"+curso.getCodigo()+"','"+curso.getNombre()+"',"+curso.getCreditos()+")";
        Conector.getConnector().ejecutarSQL(sqlQuery);

        return "El curso codigo " + curso.getCodigo() + ",se registro correctamente!";
    }

    public ArrayList<Curso> listarCursos() throws Exception {
        ArrayList<Curso> listaCursos = new ArrayList<>();
        sqlQuery="SELECT CODIGOCURSO,NOMBRE,CREDITOS FROM CURSO";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        while(rs.next()){
            Curso curso = new Curso(rs.getString("CODIGOCURSO"),rs.getString("NOMBRE"),rs.getInt("CREDITOS"));

            // Buscar los grupos y agregarlos al objeto Curso
            sqlQuery="SELECT CODIGOGRUPO, NUMERO, CODIGOCURSO FROM GRUPO WHERE CODIGOCURSO='"+curso.getCodigo()+"'";
            ResultSet rsGrupo = Conector.getConnector().ejecutarQuery(sqlQuery);

            while(rsGrupo.next()){
                curso.agregarGrupo(rsGrupo.getInt("NUMERO"),rsGrupo.getString("CODIGOGRUPO"));
            }
            listaCursos.add(curso);
        }
        return listaCursos;
    }

    public Curso buscarCurso(String codigoCurso) throws Exception {
        Curso curso = null;
        sqlQuery="SELECT CODIGOCURSO, NOMBRE, CREDITOS FROM CURSO WHERE CODIGOCURSO='"+codigoCurso+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        if(rs.next()){
            curso = new Curso(rs.getString("CODIGOCURSO"),rs.getString("NOMBRE"),rs.getInt("CREDITOS"));
        }
        return curso;
    }
}
