package cr.ac.ucenfotec.bl.entities.estudiante;

import java.util.ArrayList;

public interface IEstudianteDAO {

    public String agregarEstudiante(Estudiante estudiante) throws Exception;
    public ArrayList<Estudiante> listarEstudiantes() throws Exception;
}
