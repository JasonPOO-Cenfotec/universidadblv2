package cr.ac.ucenfotec.bl.entities.estudiante;

import cr.ac.ucenfotec.bl.entities.Persona;
import java.time.LocalDate;

public class Estudiante extends Persona {
    private int edad;
    private LocalDate fechaNacimiento;

    public Estudiante() {
    }

    public Estudiante(String identificacion, String nombre, String correo, int edad, LocalDate fechaNacimiento) {
        super(identificacion,nombre, correo);
        this.edad = edad;
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String toString() {
        return super.toString() + "," + edad +"," + fechaNacimiento;
    }
}
