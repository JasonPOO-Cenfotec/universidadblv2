package cr.ac.ucenfotec.bl.entities.estudiante;

import cr.ac.ucenfotec.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlEstudianteImpl implements IEstudianteDAO{

    private String sqlQuery;

    public String agregarEstudiante(Estudiante estudiante) throws Exception {
        sqlQuery = "INSERT INTO ESTUDIANTE VALUES ('"+estudiante.getIdentificacion()+"','"+estudiante.getNombre()+
                "','"+estudiante.getCorreo()+"',"+estudiante.getEdad()+",'"+estudiante.getFechaNacimiento()+"')";
        Conector.getConnector().ejecutarSQL(sqlQuery);

        return "El estudiante "+ estudiante.getNombre() + ", fue registrado correctamente!";
    }

    public ArrayList<Estudiante> listarEstudiantes() throws Exception {

        ArrayList<Estudiante> listaEstudiante = new ArrayList<>();
        sqlQuery="SELECT IDENTIFICACION,NOMBRE,CORREO,EDAD,FECHANACIMIENTO FROM ESTUDIANTE";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        while(rs.next()){
            Estudiante estudiante = new Estudiante(rs.getString("IDENTIFICACION"),rs.getString("NOMBRE"),rs.getString("CORREO"),
                                                   rs.getInt("EDAD"),rs.getDate("FECHANACIMIENTO").toLocalDate());
            listaEstudiante.add(estudiante);
        }
        return listaEstudiante;
    }
}
