package cr.ac.ucenfotec.bl.entities.estudiante;

import java.util.ArrayList;

public class MemoryEstudianteImpl implements IEstudianteDAO {

    public String agregarEstudiante(Estudiante estudiante) throws Exception {
        return "Se agrega el estudiante de manera correcta (Memory)!";
    }

    public ArrayList<Estudiante> listarEstudiantes() throws Exception {
        return new ArrayList<>();
    }
}
