package cr.ac.ucenfotec.util;

import java.io.FileInputStream;
import java.util.Properties;

public class Utils {

    public static String[] getProperties() throws Exception{
        String[] properties = new String[7];
        Properties p = new Properties();
        String path="src\\cr\\ac\\ucenfotec\\bd.properties";

        try{
            p.load(new FileInputStream(path));
            properties[0] = p.getProperty("driver");
            properties[1] = p.getProperty("server");
            properties[2] = p.getProperty("dataBase");
            properties[3] = p.getProperty("others");
            properties[4] = p.getProperty("user");
            properties[5] = p.getProperty("password");
            properties[6] = p.getProperty("repository");

            return properties;

        }catch(Exception e){
                throw new Exception("Error al leer el archivo de configuración de la BD");
        }
    }

}
